#include "Carambole.h"

int taille_plateau()
{
    int taillep;
    printf("1.---Modification de la taille du plateau---\n\n");
    printf("Veuillez choisir le taille du plateau desiree >> ");
    scanf("%d", &taillep);

    return(taillep);
}

int nb_jetons()
{
    int nbjeton;
    printf("2.---Modification du nombre de jetons---\n\n");
    printf("Veuillez choisir le nombre de jetons desire >> ");
    scanf("%d", &nbjeton);

    return(nbjeton);
}

int nombre_de_trou()
{
    int nbtrou;
    printf("3.---Modification du nombre de trous---\n\n");
    printf("Veuillez choisir le nombre de trous desire >> ");
    scanf("%d", &nbtrou);

    return(nbtrou);
}
char coin()
{
    char s_coin[3];
    printf("4.---Modification des style coins du plateau---\n\n");
    printf("Veuillez choisir le style coins du plateau desire >> ");
    scanf("%s", &s_coin);

    return(s_coin[0]);
}
char Vertical()
{
    char V[3];
    printf("5.---Modification du style des bords verticaux---\n\n");
    printf("Veuillez choisir le style des bords verticaux desire >> ");
    scanf("%s", &V);

    return(V[0]);
}
char Horizontal()
{
    char H[3];
    printf("6.---Modification du style des bords horizontaux---\n\n");
    printf("Veuillez choisir le style des bords horizontaux desire >> ");
    scanf("%s", &H);

    return(H[0]);
}
char sprite_jeton()
{
    char s_j[3];
    printf("7.---Modification du style de l'image des jetons---\n\n");
    printf("Veuillez choisir le style de l'image des jetons desire >> ");
    scanf("%s", &s_j);

    return(s_j[0]);
}
char sprite_palet()
{
    char sp[3];
    printf("8.---Modification du style de l'image du palet---\n\n");
    printf("Veuillez choisir le style de l'image du palet desire >> ");
    scanf("%s", &sp);

    return(sp[0]);
}
char sprite_trou()
{
    char st[3];
    printf("9.---Modification du style de l'image des trous---\n\n");
    printf("Veuillez choisir le style de l'image des trous desire >> ");
    scanf("%s", &st);

    return(st[0]);
}

config affichageMenuoption(config configuration) /* pas d'entree donc void */
{
    int affichageMenu;
    affichageMenu = 0;

    while (affichageMenu != 10)
    {
        printf("---Menu---\n\n");
        printf("1.Taille du plateau\n");
        printf("2.Nombre de jetons\n");
        printf("3.Nombre de trous\n");
        printf("4.Coins du plateau\n");
        printf("5.Bords Verticaux\n");
        printf("6.Bords Horizontaux\n");
        printf("7.Image des jetons\n");
        printf("8.Image du palet\n");
        printf("9.Image des trous\n");
        printf("10.Retour au menu principal\n\n");
        printf("Choisissez le menu d'option a modifier >> ");
        scanf("%d", &affichageMenu);
        if (affichageMenu == 1)
        {
            configuration.taille_plateau = taille_plateau();
        }
        else if (affichageMenu == 2)
        {
            configuration.nb_jetons = nb_jetons();
        }
        else if (affichageMenu == 3)
        {
            configuration.nombre_de_trou = nombre_de_trou();
        }
        else if (affichageMenu == 4)
        {
            configuration.coin = coin();
        }
        else if (affichageMenu == 5)
        {
            configuration.Vertical = Vertical();
        }
        else if (affichageMenu == 6)
        {
            configuration.Horizontal = Horizontal();
        }
        else if (affichageMenu == 7)
        {
            configuration.sprite_jeton = sprite_jeton();
        }
        else if (affichageMenu == 8)
        {
            configuration.sprite_palet = sprite_palet();
        }
        else if (affichageMenu == 9)
        {
            configuration.sprite_trou = sprite_trou();
        }
        else if (affichageMenu == 10)
        {
            modifier_Config(configuration);
            return configuration;
        }
    }
    modifier_Config(configuration);
    return configuration;
}
