#include "Carambole.h"

// Modifie le fichier de configuration
void modifier_Config(config configuration)
{
    FILE *fdmodif = fopen("config.cfg", "w");
    //fputs("", fdcheck);
    fprintf(fdmodif, "taille_plateau=%d\nb_jetons=%d\ntop_score=%d\nNombre_de_trou=%d\ncoin=%c\nVertical=%c\nHorizontal=%c\nSprite_jeton=%c\nSprite_palet=%c\nSprite_trous=%c", configuration.taille_plateau, configuration.nb_jetons, configuration.top_score, configuration.nombre_de_trou,configuration.coin, configuration.Vertical,configuration.Horizontal,configuration.sprite_jeton,configuration.sprite_palet,configuration.sprite_trou);
    fclose(fdmodif);

    fprintf(stderr,"[Debug] Configuration enregistree.");
}

// Charge la configuration depuis le fichier texte
config charger_Config()
{
    config configuration;
    FILE *fdconfig;
    char ligne[1024], *tmp;
    int k = 0;
    fdconfig = fopen("config.cfg", "r");

    // SYNTAXE Fichier config:
    // parametre=valeur

    while(fgets(ligne, 1024, fdconfig))
    {
        k++;
        tmp = strchr(ligne, '=') + 1;
        switch(k)
        {
        case 1:
            configuration.taille_plateau = atoi(tmp); // Taille du plateau
        case 2:
            configuration.nb_jetons = atoi(tmp) + 1; // Nombre de jetons
        case 3:
            configuration.top_score = atoi(tmp); // Meilleur score
        case 4:
            configuration.nombre_de_trou = atoi(tmp); // Nombre de trous
        case 5:
            configuration.coin = *tmp; // Caractère pour les coins
        case 6:
            configuration.Vertical = *tmp; // Caractère pour les murs verticaux
        case 7:
            configuration.Horizontal = *tmp; // Caractère pour les murs horizontaux
        case 8:
            configuration.sprite_jeton = *tmp; // Caractère représentant le jeton
        case 9:
            configuration.sprite_palet = *tmp; // Caractère représentant le palet
        case 10:
            configuration.sprite_trou = *tmp; // Caractère représentant les trous
        }
    }
    // Debug: affichage de la config
    //fprintf(stderr,"[Debug] T Plateau: %d\nNb jetons: %d\nTop score: %d\n", configuration.taille_plateau, configuration.nb_jetons, configuration.top_score);

    printf("Configuration chargee.\n");
    fclose(fdconfig);

    return configuration;
}

// Vérifie si le fichier de configuration existe
config check_Config()
{
    config configuration;
    if(access("config.cfg", F_OK) != 0)
    {
        // Si le fichier n'existe pas, on le créé avec une config par défaut
        FILE *fdcheck = fopen("config.cfg", "w");
        fputs("taille_plateau=15\nb_jetons=10\ntop_score=0\nNombre_de_trou=5\ncoin=c\nVertical=V\nHorizontal=H\nSprite_jeton=o\nSprite_palet=x\nSprite_trous=0", fdcheck);
        fclose(fdcheck);
        printf("[!] Le fichier de configuration est introuvable, un fichier par defaut a ete cree.\n");
        configuration = charger_Config();
    }
    else
    {
        // Si le fichier existe, on charge la config
        configuration = charger_Config();
    }
    return configuration;
}
