#include "Carambole.h"

trous *init_trou(int nombre_trou, int taille_plateau, jeton *liste_jeton, int nombre_jeton)
{
    int i;
    int k;
    int seed;
    short test;
    trous *liste_trous;

    seed = time(NULL);
    liste_trous = (trous *)malloc(sizeof(trous) * nombre_trou);
    i = 0;
    while (i != nombre_trou)
    {
        k = 0;
        test = 0;
        seed = (7 * seed + 3) % taille_plateau;
        liste_trous[i].coordonne.x =  seed;
        seed = (4 * seed + 7) % taille_plateau;
        liste_trous[i].coordonne.y =  seed;
        while (k != nombre_jeton)
        {
            if (liste_trous[i].coordonne.x == liste_jeton[k].coordonne.x && liste_trous[i].coordonne.y == liste_jeton[k].coordonne.y)
                test = 1;
            if (liste_trous[i].coordonne.y != 0 && liste_trous[i].coordonne.x != 0)
                k++;
        }
        if (test == 0)
        {
            i++;
            seed = seed * taille_plateau * 21 % taille_plateau;
        }
    }
    return liste_trous;
}

jeton *initjeton(int nb_jeton, int taille_plateau) //Fonction initialisant la liste des jetons avec leurs coordonnées de départ et leurs nombre
{
    int i;
    int j;
    int c;
    int x;
    int y;
    int millieu_plateau;
    jeton *liste_jeton;

    millieu_plateau = taille_plateau / 2;
    i = 1;
    j = 0;
    c = 1;
    x = millieu_plateau;
    y = millieu_plateau;
    liste_jeton = (jeton *)malloc(sizeof(jeton) * nb_jeton);
    while (i != nb_jeton) // Cette boucle va placer des jetons en partant du centre
    {
        while (j != c)
        {
            liste_jeton[i].coordonne.x = x; // On définit la position du jeton i.
            liste_jeton[i].coordonne.y = y;
            i++; // on passe au jeton suivant;
            y++; // on desscend d'une case à la verticale.
            j++; // on augmente le compteur
        }
        if (i >= nb_jeton)
            break;
        j = 0; // on réinitialise le compteur
        while (j != c)
        {
            liste_jeton[i].coordonne.x = x;
            liste_jeton[i].coordonne.y = y;
            i++;
            x++;
            j++;
        }
        if (i >= nb_jeton)
            break;
        j = 0;
        c++; // on augmente le nombre de fois qu'il faut répéter l'action.
        while (j != c)
        {
            liste_jeton[i].coordonne.x = x;
            liste_jeton[i].coordonne.y = y;
            i++;
            y--;
            j++;
        }
        if (i >= nb_jeton)
            break;
        j = 0;
        while (j != c)
        {
            liste_jeton[i].coordonne.x = x;
            liste_jeton[i].coordonne.y = y;
            i++;
            x--;
            j++;
        }
        if (i >= nb_jeton)
            break;
        j = 0;
        c++; // on répète nos actions jusqu'à qu'il n'y ait plus de jetons à placer.
    }
    return liste_jeton;
}

pos pos_depart_palet(int cote_plateau)// indique la position de départ du palet. Pour illustrer la fonction nous prendrons la valeur 10 pour notre paramètre.
{
    int choix1;
    pos position_palet;

    printf("\nVeuillez choisir quel face du plateau vous voulez commencer :\n1- Coin superieur gauche.\n2- Coin superieur droit.\n3- Coin inferieur gauche.\n4- Coin inferieur droit\n5- Centre haut.\n6- Centre droite.\n7- Centre bas.\n8- Centre gauche.\nChoix : ");
    choix1 = 1337;
    while (choix1 < 1 || choix1 > 8)
    {
        scanf("%d", &choix1);
        if (choix1 == 1)
        {
            position_palet.x = cote_plateau / 4;
            position_palet.y = cote_plateau / 4;
        }// le palet est en 2;2
        else if(choix1 == 2)
        {
            position_palet.x = cote_plateau - (cote_plateau / 4);
            position_palet.y = cote_plateau / 4;
        }// Le palet est en 8;2
        else if(choix1 == 3)
        {
            position_palet.x = cote_plateau / 4;
            position_palet.y = cote_plateau - (cote_plateau / 4);
        }// Le palet est en 2;8
        else if(choix1 == 4)
        {
            position_palet.x = cote_plateau - (cote_plateau / 4);
            position_palet.y = cote_plateau - (cote_plateau / 4);
        }// Le palet est en 8;8
        else if(choix1 == 5)
        {
            position_palet.x = cote_plateau / 2;
            position_palet.y = cote_plateau / 4;
        }// Le palet est en 5;2
        else if(choix1 == 6)
        {
            position_palet.x = cote_plateau - (cote_plateau / 4);
            position_palet.y = cote_plateau / 2;
        }// Le palet est en 8;5
        else if(choix1 == 7)
        {
            position_palet.x = cote_plateau / 2;
            position_palet.y = cote_plateau - (cote_plateau / 4);
        }// Le palet est en 5;8
        else if(choix1 == 8)
        {
            position_palet.x = cote_plateau / 2;
            position_palet.y = cote_plateau - (cote_plateau / 4);
        }// Le palet est en 5;8
        else
            printf("\nVeuillez choisir une valeur entre 1 et 8\n");
    }
    return position_palet;
}
