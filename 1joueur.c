#include "Carambole.h"

void ft_1joueur(config configuration)
{
    jeton *liste_jetons;
    trous *liste_trous;
    int nb_jetons_presents;

    nb_jetons_presents = configuration.nb_jetons - 1;
    liste_jetons = initjeton(configuration.nb_jetons, configuration.taille_plateau);
    liste_jetons[0].coordonne = pos_depart_palet(configuration.taille_plateau);
    liste_trous = init_trou(configuration.nombre_de_trou, configuration.taille_plateau, liste_jetons, nb_jetons_presents);
    while (nb_jetons_presents != 0)
    {
        printf("Votre palet se situe en  x = %d; y = %d\nIl reste %d jetons sur le plateau\n", liste_jetons[0].coordonne.x, liste_jetons[0].coordonne.y, nb_jetons_presents);
        affichage(liste_jetons, nb_jetons_presents, configuration, liste_trous);
        deplacement_palet(configuration, liste_trous, liste_jetons, configuration.taille_plateau, nb_jetons_presents);
        printf("*Infos :\nLe %c est votre palet\nLes %c sont les jetons\nEt les %c sont les trous.\n",configuration.sprite_palet, configuration.sprite_jeton,configuration.sprite_trou );
        nb_jetons_presents = nb_jetons_presents - plouf(liste_jetons, liste_trous, configuration);
    }
}

void ft_2joueur(config configuration)
{
    jeton *liste_jetons;
    trous *liste_trous;
    int nb_jetons_presents;
    short joueur;
    int score_joueur1;
    int score_joueur2;

    joueur = 1;
    score_joueur1 = 0;
    score_joueur2 = 0;
    nb_jetons_presents = configuration.nb_jetons - 1;
    liste_jetons = initjeton(configuration.nb_jetons, configuration.taille_plateau);
    liste_jetons[0].coordonne = pos_depart_palet(configuration.taille_plateau);
    liste_trous = init_trou(configuration.nombre_de_trou, configuration.taille_plateau, liste_jetons, nb_jetons_presents);
    while (nb_jetons_presents != 0)
    {
        printf("Au tour du joueur : %d\nVotre palet se situe en  x = %d; y = %d\nIl reste %d jetons sur le plateau\n", joueur,liste_jetons[0].coordonne.x, liste_jetons[0].coordonne.y, nb_jetons_presents);
        affichage(liste_jetons, nb_jetons_presents, configuration, liste_trous);
        deplacement_palet(configuration, liste_trous, liste_jetons, configuration.taille_plateau, nb_jetons_presents);
        printf("*Infos :\nLe %c est votre palet\nLes %c sont les jetons\nEt les %c sont les trous.\n",configuration.sprite_palet, configuration.sprite_jeton,configuration.sprite_trou );
        nb_jetons_presents = nb_jetons_presents - plouf(liste_jetons, liste_trous, configuration);
        if (joueur == 1)
        {
            joueur++;
            score_joueur1 = score_joueur1 + plouf(liste_jetons, liste_trous, configuration);
            printf("\nVotre score est de : %d.", score_joueur1);
        }
        else
        {
            joueur--;
            score_joueur2 = score_joueur2 + plouf(liste_jetons, liste_trous, configuration);
            printf("\nVotre score est de : %d.", score_joueur2);
        }
    }
}
