#include "Carambole.h"

// Lance le palet dans la direction et à la vitesse donnée
// Déplace le palet aux coordonnées x, y indiquées
jeton *deplacement_palet(config configuration,trous *liste_trous, jeton *liste_jeton, int cote_plateau, int nb_jetons_presents) //Réfléchir au retour de cette fonction
{
    // BOUCLE INFINI ICI
    int k;
    int j;

    j = 0;
    k = 0;
    printf("\nChoisissez une direction : (entrez 8 ou plus pour afficher la liste des directions)\n0:Haut, 1:Haut_droite, 2:Droite, 3:Bas_droite, 4:Bas, 5:Bas_gauche, 6:Gauche, 7:Haut_gauche\nDirection : ");
    scanf("%d", &liste_jeton[0].direction);
    while (liste_jeton[0].direction < 0 || liste_jeton[0].direction >= 7)
    {
        scanf("%d",&liste_jeton[0].direction);
        if (liste_jeton[0].direction < 0 || liste_jeton[0].direction > 7)
            printf("\nVeuillez choisir une direction valable. (0 a 7)\n");
    }
    while(liste_jeton[0].vitesse <= 0)
    {
        printf("\nChoisissez une vitesse (vitesse = nombre de cases parcourues)");
        scanf("%d",&liste_jeton[0].vitesse);
        if (liste_jeton[0].vitesse < 0)
            printf("\nVeuillez choisir une vitesse positive.\n");
    }
    while(liste_jeton[k].vitesse > 0) //Tant que les coord du palet ne touche pas les coord d'un jeton et que la vitesse du palet n'est pas égal à 0;
    {
        if(liste_jeton[k].coordonne.x == 0 || liste_jeton[k].coordonne.x == cote_plateau || liste_jeton[k].coordonne.y == 0 || liste_jeton[k].coordonne.y == cote_plateau) //si le palet touche le bord du plateau
        {
            if(liste_jeton[k].direction == 0 || liste_jeton[k].direction == 2 )
            {
                liste_jeton[k].direction=liste_jeton[k].direction + 4;
            }
            else if(liste_jeton[k].direction == 4 || liste_jeton[k].direction == 6)
            {
                liste_jeton[k].direction=liste_jeton[k].direction - 4;
            }
            else if(liste_jeton[k].direction == 1 && liste_jeton[k].coordonne.x == cote_plateau) //si le palet a une direction de haut à droite et que qu'il se situe sur le bord droit tu plateau
            {
                liste_jeton[k].direction = 7; //il repartira forcément en haut à gauche.
            }
            else if(liste_jeton[k].direction == 1 && liste_jeton[k].coordonne.y == 0)
            {
                liste_jeton[k].direction = 3;
            }
            else if(liste_jeton[k].direction == 3 && liste_jeton[k].coordonne.x == cote_plateau)
            {
                liste_jeton[k].direction = 5;
            }
            else if(liste_jeton[k].direction == 5 && liste_jeton[k].coordonne.y == cote_plateau)
            {
                liste_jeton[k].direction = 7;
            }
            else if(liste_jeton[k].direction == 5 && liste_jeton[k].coordonne.x == 0) //si le palet à une direction de bas à gauche et qu'il touche le coté gauche du plateau.
            {
                liste_jeton[k].direction = 3; //Alors il repart en bas à droite
            }
            else if(liste_jeton[k].direction == 7 && liste_jeton[k].coordonne.y == 0)
            {
                liste_jeton[k].direction = 5;
            }
            else if(liste_jeton[k].direction == 7 && liste_jeton[k].coordonne.x == 0)
            {
                liste_jeton[k].direction = 1;
            }
        }

        if(liste_jeton[k].direction == 0)  //En haut
        {
            liste_jeton[k].coordonne.y = liste_jeton[k].coordonne.y - 1;
        }
        else if(liste_jeton[k].direction == 1) //En haut à droite
        {
            liste_jeton[k].coordonne.y = liste_jeton[k].coordonne.y - 1;
            liste_jeton[k].coordonne.x = liste_jeton[k].coordonne.x + 1;
        }
        else if(liste_jeton[k].direction == 2)  //Droite
        {
            liste_jeton[k].coordonne.x = liste_jeton[k].coordonne.x + 1;
        }
        else if(liste_jeton[k].direction == 3)  //Bas Droite
        {
            liste_jeton[k].coordonne.x = liste_jeton[k].coordonne.x + 1;
            liste_jeton[k].coordonne.y = liste_jeton[k].coordonne.y + 1;
        }
        else if(liste_jeton[k].direction == 4)  //Bas
        {
            liste_jeton[k].coordonne.y = liste_jeton[k].coordonne.y + 1;
        }
        else if(liste_jeton[k].direction == 5)  //Bas gauche
        {
            liste_jeton[k].coordonne.x = liste_jeton[k].coordonne.x - 1;
            liste_jeton[k].coordonne.y = liste_jeton[k].coordonne.y + 1;
        }
        else if(liste_jeton[k].direction == 6)  //Gauche
        {
            liste_jeton[k].coordonne.x = liste_jeton[k].coordonne.x - 1;
        }
        else if(liste_jeton[k].direction == 7)  //Haut gauche
        {
            liste_jeton[k].coordonne.x = liste_jeton[k].coordonne.x - 1;
            liste_jeton[k].coordonne.y = liste_jeton[k].coordonne.y - 1;
        }
        //affichage(liste_jeton, nb_jetons_presents, configuration, liste_trous);											//Image par seconde
        while (j != nb_jetons_presents)
        {
            if(liste_jeton[k].coordonne.x == liste_jeton[j].coordonne.x && liste_jeton[k].coordonne.y == liste_jeton[j].coordonne.y && j != k)
            {
                //Exemple lorsque le jeton 0 rencontre le jeton 6.
                //printf("Colision entre les jetons %d et %d\n", j, k);
                liste_jeton[j].vitesse = liste_jeton[k].vitesse; //Le jeton 6 a la vitesse du jeton 0
                //printf("Vitesse j = %d, vitesse k = %d\n", liste_jeton[j].vitesse, liste_jeton[k].vitesse);
                liste_jeton[j].direction = liste_jeton[k].direction;// Le jeton 6 a la direction du jeton 0
                liste_jeton[k].vitesse = 0;// Le jeton 0 a 0 de vitesse.
                liste_jeton[k].direction = 0;// Le jeton 0 a 0 de direction
                //printf("Nouvelle Vitesse j = %d, nouvelle vitesse k = %d\n", liste_jeton[j].vitesse, liste_jeton[k].vitesse);
                //printf("%d = %d\n", k, j);
                k = j;//Le jeton regardé est égal à 6.
                //printf("k = %d, j = %d\n", k, j);
                //printf("Nouvelle Vitesse j = %d, nouvelle vitesse k = %d\n", liste_jeton[j].vitesse, liste_jeton[k].vitesse);
            }
            j++;
        }
        j = 0;
        liste_jeton[k].vitesse = liste_jeton[k].vitesse - 1;
    }
    return liste_jeton;
}

int plouf(jeton *liste_jeton, trous *liste_trous, config configuration) //Convertir cette fonction pour qu'elle utilise les structures
{
    int nb_jeton;
    int i;
    int j;

    i = 1;
    j = 0;
    nb_jeton = 0;
    while (i != configuration.nb_jetons)
    {
        while (j != configuration.nombre_de_trou)
        {
            if(liste_jeton[i].coordonne.x == liste_trous[j].coordonne.x && liste_jeton[i].coordonne.y == liste_trous[j].coordonne.y)
            {
                nb_jeton++;
                liste_jeton[i].coordonne.x = 1337;
                liste_jeton[i].coordonne.y = 1337;
            }
            j++;
        }
        j = 0;
        i++;
    }
    return nb_jeton;
}
