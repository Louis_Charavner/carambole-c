#ifndef __MAIN_H__
#define __MAIN_H__

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

typedef struct pos pos;
typedef struct jeton jeton;
typedef struct config config;
typedef struct trou trous;

// Initialise la structure des positions (x, y)
struct pos
{
    int x;
    int y;
};

// Initialise la structure d'un jeton
struct jeton
{
    pos coordonne;
    int vitesse;
    short direction;
};

struct config // Si vous voulez rajouter de la config c'est ici.
{
    int taille_plateau;
    int nb_jetons;
    int top_score;
    int nombre_de_trou;
    char coin;
    char Vertical;
    char Horizontal;
    char sprite_jeton;
    char sprite_palet;
    char sprite_trou;
};

struct trou
{
    pos coordonne;
    char sprite_trou;
};

trous *init_trou(int nombre_trou, int taille_plateau, jeton *liste_jeton, int nombre_jeton);
jeton *deplacement_palet(config configuration,trous *liste_trous, jeton *liste_jeton, int cote_plateau, int nb_jetons_presents);
jeton *initjeton(int nb_jeton, int taille_plateau);
config affichageMenuoption(config configuration);
config charger_Config();
config check_Config();
pos pos_depart_palet(int cote_plateau);
void affichage(jeton *jeton_present, int nombredejeton, config configuration, trous *liste_trous);
void ft_1joueur(config configuration);
void ft_2joueur(config configuration);
void modifier_Config(config configuration);
void afficher_MenuPrinc();
char **define_char(char **tab, char coin, char bordH, char bordV, int size);
char **ft_malloc(int size);
char coin();
char Vertical();
char Horizontal();
char sprite_jeton();
char sprite_palet();
char sprite_trou();
short jeu();
int plouf(jeton *liste_jeton, trous *liste_trous, config configuration);
int nombre_de_trou();
int taille_plateau();
int nb_jetons();
int main();

# endif

// Date du fichier 10/11/2016 - 16:15
