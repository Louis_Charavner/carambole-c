#include "Carambole.h"

void afficher_MenuPrinc()
{
    printf("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n*****La Carambole*****\n____________\n\n*1 1 Joueur\n*2 2 Joueur\n*3 Options\n*4 Quitter\n____________\nChoisissez un mode de jeu : ");
}

// Début de jeu
short jeu(int mode)
{
    config configuration;
    configuration = check_Config();
    afficher_MenuPrinc(); //Affichage du menu.
    scanf("%d", &mode);
    if (mode == 1)
    {
        ft_1joueur(configuration);
        configuration = charger_Config();
    }
    else if (mode == 2)
    {
        ft_2joueur(configuration);
        configuration = charger_Config();
    }
    else if (mode == 3)
    {
        configuration = affichageMenuoption(configuration);
        jeu(0);
    }
    else if (mode == 4) // Quitter
        return 0;
    else
    {
        printf("\nChoisissez une valeur entre 1 et 4 :");
        jeu(0);
    }
}

// Fonction principale
int main()
{
    jeu(0);
    return 0;
}
