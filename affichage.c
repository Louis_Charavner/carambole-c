#include "Carambole.h"

char **define_char(char **tab, char coin, char bordH, char bordV, int size)
{
    int j, i;

    i = 0;
    j = 0;
    while (i != size)
    {
        while (j != size)
        {
            if (i == 0 || i == size - 1) //si on est à la ligne du haut soit la ligne du bas
                if (j == 0 || j == size - 1) // et si on est au premier caractère ou au dernier caractère
                    tab[i][j] = coin; //alors ces char seront des coins
            if (i != size - 1 && i != 0) //Si on est pas à la dernière ligne ni à la première
                if (j == 0 || j == size - 1) //et si on est au premier ou au dernier char
                    tab[i][j] = bordV; //Alors le char est un bord vertical
            if (j != 0 && j != size - 1)
                if (i == 0 || i == size - 1)
                    tab[i][j] = bordH;
            j++;
        }
        j = 0;
        i++;
    }
    return tab;
}

char **ft_malloc(int size)
{
    char **tab;
    int i;

    i = 0;
    tab = (char **)malloc(sizeof(char *) * size);
    while (i != size)
    {
        tab[i] = (char *)malloc(sizeof(char) * size);
        i++;
    }
    return tab;
}

// Gère l'affichage du plateau et des jetons
void affichage(jeton *jeton_present, int nombredejeton, config configuration, trous *liste_trous)
{
    int i;
    int j;
    int p;
    int l;
    short test;
    char **plateau;

    i = 0;
    j = 0;
    l = 0;
    plateau = ft_malloc(configuration.taille_plateau);
    plateau = define_char(plateau, configuration.coin, configuration.Horizontal, configuration.Vertical, configuration.taille_plateau); // Les char sont des valeurs de test ont les récuperera dans un fichier texte.
    while ( i != configuration.taille_plateau) // Parcour le tableau à la verticale
    {
        while (j != configuration.taille_plateau) // Parcour le tableau à l'horizontale
        {
            test = 1;
            p = 0;
            while ( p != nombredejeton) // Parcour la liste des jetons.
            {
                if (jeton_present[p].coordonne.x == j && jeton_present[p].coordonne.y == i)
                {
                    test = 0;
                    break;
                }
                p++;
            }
            l = 0;
            while ( l != configuration.nombre_de_trou)
            {
                if (liste_trous[l].coordonne.x == j && liste_trous[l].coordonne.y == i && test != 0)
                {
                    test = 2;
                    break;
                }
                l++;
            }
            if (test == 1)
                printf("%c", plateau[i][j]);
            else if (p == 0)
                printf("%c", configuration.sprite_palet); // caractère des palet.
            else if (test == 2)
                printf("%c", configuration.sprite_trou); //caractère des trous.
            else if (test == 0 && p != 0)
                printf("%c", configuration.sprite_jeton); // caractère des jetons.
            j++;
            //printf("test = %d", test);
        }
        printf("\n");
        j = 0;
        i++;
    }
    free(plateau);
}
